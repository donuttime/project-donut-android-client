package com.donut.donutandroidclient.jsonconvertion;

import com.donut.donutandroidclient.model.DestinationLocation;
import com.google.gson.Gson;

/**
 * Created by ilan on 7/16/16.
 */
public class DestinationJsonConverter extends JsonConverter<DestinationLocation> {

    Gson gson = new Gson();

    @Override
    public String toJson(DestinationLocation input) {
        return gson.toJson(input);
    }

    @Override
    public DestinationLocation fromJson(String json) {
        return gson.fromJson(json, DestinationLocation.class);
    }
}
