package com.donut.donutandroidclient.jsonconvertion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;

/**
 * Created by ilan on 7/16/16.
 */
public abstract class JsonConverter<T> {
    public String toJson(Collection<? extends T> allObjs) throws JSONException {
        StringBuilder builder = new StringBuilder("[");
        for (T obj : allObjs) {
            String objJson = toJson(obj);
            builder.append(objJson + ",");
        }
        builder.deleteCharAt(builder.length() - 1);
        builder.append("]");
        return builder.toString();
    }

    public abstract String toJson(T input) throws JSONException;

    public void fromJson(String json, Collection<T> output) throws JSONException {

        JSONArray array = new JSONArray(json);
        int length = array.length();
        for (int i = 0; i < length; i++) {
            JSONObject obj = array.getJSONObject(i);
            T convertedObj = fromJson(obj.toString());
            output.add(convertedObj);
        }

    }

    public abstract T fromJson(String json) throws JSONException;
}
