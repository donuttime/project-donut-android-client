package com.donut.donutandroidclient.displayresults;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.donut.donutandroidclient.R;
import com.donut.donutandroidclient.model.LocationPoint;
import com.donut.donutandroidclient.model.TravelRoute;

import java.util.List;

/**
 * Created by ilan on 8/14/16.
 */
public class ResultsAdapter extends BaseExpandableListAdapter {

    private List<TravelRoute> routes;
    private Context context;

    public ResultsAdapter(Context context, List<TravelRoute> routes) {
        this.routes = routes;
        this.context = context;
    }

    @Override
    public int getGroupCount() {
        return routes.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return routes.get(groupPosition).getRoute().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return routes.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return routes.get(groupPosition).getRoute().get(childPosition).getPt();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return Long.valueOf(groupPosition + "000" + childPosition);
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        TravelRoute item = (TravelRoute) getGroup(groupPosition);
        String destName = item.getDestination().getName();

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.results_destination, null);
            TextView titleView = (TextView) convertView.findViewById(R.id.destname);
            convertView.setTag(titleView);
        }

        TextView titleView = (TextView) convertView.getTag();
        titleView.setText(destName);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        LocationPoint pt = (LocationPoint) getChild(groupPosition, childPosition);
        String ptName = pt.getName();

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.results_journey_item, null);
            TextView titleView = (TextView) convertView.findViewById(R.id.journeytitle);
            convertView.setTag(titleView);
        }

        TextView itemView = (TextView) convertView.getTag();
        itemView.setText(ptName);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
