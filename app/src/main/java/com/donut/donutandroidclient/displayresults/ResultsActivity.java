package com.donut.donutandroidclient.displayresults;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.donut.donutandroidclient.R;
import com.donut.donutandroidclient.jsonconvertion.TravelRouteJsonConverter;
import com.donut.donutandroidclient.model.TravelRoute;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResultsActivity extends AppCompatActivity {

    public static final String JSON_KEY = "RouteJson";

    @BindView(R.id.displayresultslist)
    ExpandableListView resultsView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        ButterKnife.bind(this);

        resultsView.setAdapter(getAdapter(getJson(savedInstanceState)));
    }

    private String getJson(Bundle savedInstanceState) {
        if (savedInstanceState != null && savedInstanceState.getString(JSON_KEY) != null) {
            return savedInstanceState.getString(JSON_KEY);
        }

        Intent intent = getIntent();
        String intentJson = intent.getStringExtra(JSON_KEY);
        if (intentJson != null) return intentJson;

        return "";
    }

    private ExpandableListAdapter getAdapter(String json) {
        if (json == null || json.length() <= 1) {
            Toast.makeText(this, "JSON is null.", Toast.LENGTH_LONG).show();
            return new ResultsAdapter(this, new ArrayList<TravelRoute>());
        }
        TravelRouteJsonConverter converter = new TravelRouteJsonConverter();
        List<TravelRoute> routes = new ArrayList<>();
        try {
            converter.fromJson(json, routes);
        } catch (JSONException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            Log.e(getClass().getName(), String.format("JSON Error:\n%s\n\nJSON:\n%s", e.getMessage(), json));
            return new ResultsAdapter(this, new ArrayList<TravelRoute>());
        }

        return new ResultsAdapter(this, routes);
    }


}
