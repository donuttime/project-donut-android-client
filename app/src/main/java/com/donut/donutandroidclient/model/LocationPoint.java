package com.donut.donutandroidclient.model;


public interface LocationPoint {
    String getName();

    LocationType getType();

    double[] getCoordinates();
}
