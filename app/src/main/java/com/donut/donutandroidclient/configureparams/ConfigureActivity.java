package com.donut.donutandroidclient.configureparams;

import android.Manifest;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.donut.donutandroidclient.R;
import com.donut.donutandroidclient.loaddata.LoaderActivity;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class ConfigureActivity extends AppCompatActivity {

    private static final int START_TIME_DIALOG_ID = 999;
    private static final int END_TIME_DIALOG_ID = 2344;

    @BindView(R.id.endtimeview)
    TextView endTimePicker;
    @BindView(R.id.starttimeview)
    TextView startTimePicker;
    @BindView(R.id.customlocationlayout)
    LinearLayout customLocationLayout;
    @BindView(R.id.customstartlocation)
    EditText customLocationView;
    @BindView(R.id.locationtypepicker)
    EditText locationType;

    int selectedEndTimeHour = -1;
    int selectedEndTimeMinute = -1;
    int selectedStartTimeHour = -1;
    int selectedStartTimeMinute = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configure);
        ButterKnife.bind(this);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case END_TIME_DIALOG_ID:
                return new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        selectedEndTimeHour = hourOfDay;
                        selectedEndTimeMinute = minute;
                        endTimePicker.setText(String.format("%d:%2d", hourOfDay, minute));
                    }
                }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), false);
            case START_TIME_DIALOG_ID:
                return new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        selectedStartTimeHour = hourOfDay;
                        selectedStartTimeMinute = minute;
                        startTimePicker.setText(String.format("%d:%2d", hourOfDay, minute));

                    }
                }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), false);
            default:
                return super.onCreateDialog(id);
        }
    }

    @OnClick(R.id.endtimeview)
    void selectTime() {
        showDialog(END_TIME_DIALOG_ID);
    }

    @OnClick(R.id.starttimeview)
    void selectStartTime() {
        showDialog(START_TIME_DIALOG_ID);
    }

    @OnCheckedChanged(R.id.currentlocationbox)
    void toggleStartLocationPicker(boolean checked) {
        if (checked) {
            customLocationLayout.setVisibility(View.GONE);
        } else {
            customLocationLayout.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.submitbutton)
    void submit() {
        double[] latLong = getLatLong();
        if (latLong[0] == Double.MIN_VALUE || latLong[1] == Double.MIN_VALUE || selectedEndTimeHour < 0 || selectedEndTimeMinute < 0 || locationType.length() == 0) {
            Toast.makeText(this, "Need to answer everything before submitting.", Toast.LENGTH_LONG).show();
            Log.d(getClass().getName(), String.format("Args:\n h = %d\n m = %d lat = %f lng = %f type = %s", selectedEndTimeHour, selectedEndTimeMinute, latLong[0], latLong[1], locationType));
            return;
        }
        Intent loaderIntent = new Intent(this, LoaderActivity.class);
        loaderIntent.putExtra(LoaderActivity.TYPE_TAG, locationType.getText().toString());
        loaderIntent.putExtra(LoaderActivity.LAT_TAG, latLong[0]);
        loaderIntent.putExtra(LoaderActivity.LONG_TAG, latLong[1]);
        loaderIntent.putExtra(LoaderActivity.TIME_DELTA_TAG, getEndTime() - getStartTime());
        loaderIntent.putExtra(LoaderActivity.START_TIME_TAG, getStartTime());
        startActivity(loaderIntent);
    }

    private long getEndTime() {
        if (selectedEndTimeHour < 0 || selectedEndTimeMinute < 0) return -1;
        Calendar arrive = Calendar.getInstance();
        arrive.set(Calendar.HOUR_OF_DAY, selectedEndTimeHour);
        arrive.set(Calendar.MINUTE, selectedEndTimeMinute);

        return arrive.getTimeInMillis();
    }

    private long getStartTime() {
        if (selectedStartTimeHour < 0 || selectedEndTimeHour < 0) return System.currentTimeMillis();
        Calendar start = Calendar.getInstance();
        start.set(Calendar.HOUR_OF_DAY, selectedStartTimeHour);
        start.set(Calendar.MINUTE, selectedStartTimeMinute);

        return start.getTimeInMillis();
    }

    private double[] getLatLong() {
        if (customLocationLayout.getVisibility() == View.GONE) {
            return getCurrentLatLong();
        }

        String addressString = customLocationView.getText().toString();

        Geocoder geocoder = new Geocoder(this);
        List<Address> results;
        try {
            results = geocoder.getFromLocationName(addressString, 1);
        } catch (IOException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT);
            return new double[]{Double.MIN_VALUE, Double.MIN_VALUE};
        }
        return new double[]{results.get(0).getLatitude(), results.get(0).getLongitude()};

    }

    private double[] getCurrentLatLong() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Location permission check failed.", Toast.LENGTH_LONG).show();
            return new double[]{Double.MIN_VALUE, Double.MIN_VALUE};
        }
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        double longitude = location.getLongitude();
        double latitude = location.getLatitude();
        return new double[]{latitude, longitude};
    }

}
