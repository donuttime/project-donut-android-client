package com.donut.donutandroidclient.loaddata;

import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by ilan on 8/14/16.
 */


public interface DonutRetrofit {

    @GET("routes")
    Observable<ResponseBody> getRoutes(@Query("latitude") double lat, @Query("longitude") double lng,
                                       @Query("starttime") long startTimeInSeconds, @Query("timedelta") long timeDeltaInSeconds,
                                       @Query("type") String locationType);
}
