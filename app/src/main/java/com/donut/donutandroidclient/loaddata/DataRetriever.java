package com.donut.donutandroidclient.loaddata;

import com.donut.donutandroidclient.jsonconvertion.TravelRouteJsonConverter;
import com.donut.donutandroidclient.model.TravelRoute;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by ilan on 8/14/16.
 */
public class DataRetriever {

    private static final String BASE_URL = "http://projectdonut.ddns.net:1997";

    public Observable<? extends List<TravelRoute>> queryData(double[] latLong, long startTimeMilli, long deltaMilli, String locationType) {

        if (latLong == null || latLong.length != 2 || startTimeMilli < 0 || deltaMilli < 0 || locationType == null || locationType.length() == 0) {
            return Observable.just(new ArrayList<TravelRoute>());
        }

        OkHttpClient delayer = new OkHttpClient.Builder().readTimeout(120, TimeUnit.SECONDS).connectTimeout(60, TimeUnit.SECONDS).build();

        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(delayer)
                .baseUrl(BASE_URL)
                .build();

        DonutRetrofit donutRetrofit = retrofit.create(DonutRetrofit.class);

        return donutRetrofit.getRoutes(latLong[0], latLong[1], startTimeMilli / 1000, deltaMilli / 1000, locationType)
                .map(new Func1<ResponseBody, List<TravelRoute>>() {
                    @Override
                    public List<TravelRoute> call(ResponseBody s) {
                        List<TravelRoute> rval = new ArrayList<>();
                        TravelRouteJsonConverter converter = new TravelRouteJsonConverter();
                        try {
                            converter.fromJson(s.string(), rval);
                        } catch (JSONException e) {
                            return new ArrayList<>();
                        } catch (IOException e) {
                            return new ArrayList<>();
                        }
                        return rval;
                    }
                });
    }
}
