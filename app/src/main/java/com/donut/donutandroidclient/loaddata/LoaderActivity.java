package com.donut.donutandroidclient.loaddata;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.donut.donutandroidclient.R;
import com.donut.donutandroidclient.displayresults.ResultsActivity;
import com.donut.donutandroidclient.jsonconvertion.TravelRouteJsonConverter;
import com.donut.donutandroidclient.model.TravelRoute;

import org.json.JSONException;

import java.util.List;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class LoaderActivity extends AppCompatActivity {

    public static String START_TIME_TAG = "starttime";
    public static String LAT_TAG = "latitude";
    public static String LONG_TAG = "longitude";
    public static String TYPE_TAG = "type";
    public static String TIME_DELTA_TAG = "timedelta";


    private Subscription dlSub;

    private void initTask() {

        Intent intent = getIntent();

        long startTime = intent.getLongExtra(START_TIME_TAG, -1);
        if (startTime < 0) finish();

        long timeDelta = intent.getLongExtra(TIME_DELTA_TAG, -1);
        if (timeDelta < 0) finish();


        double lat = intent.getDoubleExtra(LAT_TAG, Double.MIN_VALUE);
        if (lat == Double.MIN_VALUE) finish();

        double lng = intent.getDoubleExtra(LONG_TAG, Double.MIN_VALUE);
        if (lng == Double.MIN_VALUE) finish();

        String type = intent.getStringExtra(TYPE_TAG);
        if (type == null || type.length() < 2) finish();

        Observable<? extends List<TravelRoute>> dlStream = new DataRetriever().queryData(new double[]{lat, lng}, startTime, timeDelta, type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        if (dlSub != null && !dlSub.isUnsubscribed()) dlSub.unsubscribe();

        dlSub = dlStream
                .subscribe(new Action1<List<TravelRoute>>() {
                               @Override
                               public void call(List<TravelRoute> travelRoutes) {
                                   TravelRouteJsonConverter converter = new TravelRouteJsonConverter();
                                   String json;
                                   try {
                                       json = converter.toJson(travelRoutes);
                                   } catch (JSONException e) {
                                       handlerError(e);
                                       LoaderActivity.this.finish();
                                       return;
                                   }

                                   Intent displayIntent = new Intent(LoaderActivity.this, ResultsActivity.class);
                                   displayIntent.putExtra(ResultsActivity.JSON_KEY, json);
                                   LoaderActivity.this.startActivity(displayIntent);
                                   finish();

                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                handlerError(throwable);
                                finish();
                            }
                        });
    }


    private void handlerError(Throwable e) {
        Log.e(getClass().getName(), e.getMessage());
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loader);
        initTask();

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        initTask();
    }

    @Override
    protected void onPause() {
        if (dlSub != null && !dlSub.isUnsubscribed()) dlSub.unsubscribe();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (dlSub != null && !dlSub.isUnsubscribed()) dlSub.unsubscribe();
        super.onDestroy();
    }
}
